package org.antwalk;

import java.util.List;

import org.antwalk.dao.BookDaoImpl;
import org.antwalk.dao.TeacherDaoImpl;
import org.antwalk.model.Book;
import org.antwalk.model.Teacher;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	public static void main(String[] args) {
		ApplicationContext context=new ClassPathXmlApplicationContext("beans.xml");
//		BookDaoImpl book=(BookDaoImpl) context.getBean("bookDaoImpl");
		TeacherDaoImpl teacherDaoImpl = (TeacherDaoImpl) context.getBean("TeacherDaoImpl");
		System.out.println("Adding records");
		
		teacherDaoImpl.create("Bimal", "Kolkat");
		teacherDaoImpl.create("Kamal", "Mumbai");

		
//		book.create("The Firm", "John Grisham", 100);
//		book.create("The Souffle", "Anand Ranganatham", 195);
//		book.create("The Parther", "John Grisham", 200);
//		book.create("The Deal", "John Doe", 290);
//	
		System.out.println("Listing the teachers");
		List<Teacher> teachers=teacherDaoImpl.listTeachers();
		for(Teacher teacher:teachers)
		{
			System.out.println("Id: "+teacher.getId());
			System.out.println("Name: "+teacher.getName());
			System.out.println("Address: "+teacher.getAddress());
		}
		
	}

}

package org.antwalk.dao;

import java.util.List;

import javax.sql.DataSource;

import org.antwalk.model.Book;
import org.springframework.jdbc.core.JdbcTemplate;

public class BookDaoImpl implements BookDao{
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	@Override
	public void setDataSource(DataSource ds) {
		this.dataSource =ds;
		this.jdbcTemplate = new JdbcTemplate(ds);
	}

	@Override
	public void create(String title, String author, int price) {
		String query = "insert into book(title,author,price) values(?,?,?)";
		jdbcTemplate.update(query,title, author,price);
		
	}

	@Override
	public List<Book> listBook() {
		String query = "select * from book";
		List<Book> books = jdbcTemplate.query(query, new BookMapper());
		return books;
	}

}
